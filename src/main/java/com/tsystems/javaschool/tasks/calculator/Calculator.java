package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;
import java.util.NoSuchElementException;




public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
            LinkedList<Double> stack = new LinkedList<>();
            LinkedList<Character> operators = new LinkedList<Character>();
            DecimalFormat df = new DecimalFormat("##.####");
            DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
            dfs.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(dfs);
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (isDelim(c))
                    continue;
                if (c == '(')
                    operators.add('(');
                else if (c == ')') {
                    while (operators.getLast() != '(')
                        processOperator(stack, operators.removeLast());
                    operators.removeLast();
                } else if (isOperator(c)) {
                    while (!operators.isEmpty() && priority(operators.getLast()) >= priority(c))
                        processOperator(stack, operators.removeLast());
                    operators.add(c);
                } else {
                    String operand = "";
                    while (i < statement.length() && Character.isDigit(statement.charAt(i)) | statement.charAt(i) == '.') {
                        operand += statement.charAt(i++);
                    }
                    i--;
                    stack.add(Double.parseDouble(operand));
                }
            }
            while (!operators.isEmpty())
                processOperator(stack, operators.removeLast());
            return df.format(stack.get(0));
        } catch (Exception ex) {
            return null;
        }
    }

    static boolean isDelim(char c) {
        return c == ' ';
    }

    static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }

    static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
            case '%':
                return 2;
            default:
                return -1;
        }
    }

    static void processOperator(LinkedList<Double> stack, char operation) {
        double first = stack.removeLast();
        double second = stack.removeLast();
        switch (operation) {
            case '+':
                stack.add(second + first);
                break;
            case '-':
                stack.add(second - first);
                break;
            case '*':
                stack.add(second * first);
                break;
            case '/':
                if (first == 0) {
                    stack.clear();
                    stack = null;
                }
                stack.add(second / first);
                break;
            case '%':
                stack.add(second % first);
                break;
        }
    }


}
