package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        // TODO : Implement your solution here
        int height = 0;
        int width;

        int size = 0;
        int count = 1;

        boolean canBuild = false;

        if(inputNumbers.size()>1000){
            throw new CannotBuildPyramidException("Can't build that big");
        }
        else {
            if (inputNumbers.contains(null)) {
                throw new CannotBuildPyramidException("contains null element");
            } else {
                Collections.sort(inputNumbers);
            }
        }
        for (int i : inputNumbers) {
            size = size + count;
            height++;
            if (size == inputNumbers.size()) {
                canBuild = true;
                break;
            }
            count++;
        }

        int[][] intArray;
        if (canBuild == true) {
            width = height * 2 - 1;
            int k = inputNumbers.size() - 1;


            intArray = new int[height][width];
            for (int i = height - 1; i >= 0; i--) {
                int counter = (height - 1) - i;
                int numberOfZeros = counter;
                for (int j = width - 1; j >= 0; j--) {
                    if ((j == width - 1 - counter) && (j >= numberOfZeros)) {
                        intArray[i][j] = inputNumbers.get(k);
                        k--;
                        counter = counter + 2;
                    }
                }
            }
        } else {
            throw new CannotBuildPyramidException("cannot build pyramid");
        }
        return intArray;
    }


}
